﻿namespace lv6analizazad2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tB_1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.attempts = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.remainingAttempts = new System.Windows.Forms.Label();
            this.tryLetter = new System.Windows.Forms.Button();
            this.quit = new System.Windows.Forms.Button();
            this.tB_2 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // tB_1
            // 
            this.tB_1.Location = new System.Drawing.Point(67, 35);
            this.tB_1.Name = "tB_1";
            this.tB_1.Size = new System.Drawing.Size(26, 20);
            this.tB_1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Slovo:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Pokušaji:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // attempts
            // 
            this.attempts.AutoSize = true;
            this.attempts.Location = new System.Drawing.Point(80, 80);
            this.attempts.Name = "attempts";
            this.attempts.Size = new System.Drawing.Size(0, 13);
            this.attempts.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Preostalo pokušaja:";
            // 
            // remainingAttempts
            // 
            this.remainingAttempts.AutoSize = true;
            this.remainingAttempts.Location = new System.Drawing.Point(130, 115);
            this.remainingAttempts.Name = "remainingAttempts";
            this.remainingAttempts.Size = new System.Drawing.Size(0, 13);
            this.remainingAttempts.TabIndex = 5;
            // 
            // tryLetter
            // 
            this.tryLetter.Location = new System.Drawing.Point(161, 32);
            this.tryLetter.Name = "tryLetter";
            this.tryLetter.Size = new System.Drawing.Size(75, 23);
            this.tryLetter.TabIndex = 6;
            this.tryLetter.Text = "Pokušaj!";
            this.tryLetter.UseVisualStyleBackColor = true;
            this.tryLetter.Click += new System.EventHandler(this.tryLetter_Click);
            // 
            // quit
            // 
            this.quit.Location = new System.Drawing.Point(244, 194);
            this.quit.Name = "quit";
            this.quit.Size = new System.Drawing.Size(75, 23);
            this.quit.TabIndex = 7;
            this.quit.Text = "Quit";
            this.quit.UseVisualStyleBackColor = true;
            this.quit.Click += new System.EventHandler(this.quit_Click);
            // 
            // tB_2
            // 
            this.tB_2.Location = new System.Drawing.Point(27, 158);
            this.tB_2.Name = "tB_2";
            this.tB_2.Size = new System.Drawing.Size(294, 20);
            this.tB_2.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(331, 229);
            this.Controls.Add(this.tB_2);
            this.Controls.Add(this.quit);
            this.Controls.Add(this.tryLetter);
            this.Controls.Add(this.remainingAttempts);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.attempts);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tB_1);
            this.Name = "Form1";
            this.Text = "Vješala";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tB_1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label attempts;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label remainingAttempts;
        private System.Windows.Forms.Button tryLetter;
        private System.Windows.Forms.Button quit;
        private System.Windows.Forms.TextBox tB_2;
    }
}

